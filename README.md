An app to display Abercrombie and Fitch's promotion items. 
Clicking on a promotion Item will display the promotion's detail
The promotion detail screen contains a button to target the promotion webpage. 


To import the app into android studio:
1. Start Android Studio
2. From VCS Menu select check out from version control
3. select git
4. insert the link: https://gitlab.com/HeshamFas/anfpromotionalapp.git
5. update with gradle

This Project is build with android studio 2.1.1
gradle version: 2.1.0

   your root build.gradle file should include: 
    dependencies {
        classpath 'com.android.tools.build:gradle:2.1.0'
    }
    
