package com.hesham.anf.afpromotional;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;
import com.hesham.anf.afpromotional.data.PromotionsRepository;
import com.hesham.anf.afpromotional.service.BitmapLruCache;

import android.app.Application;
import android.content.Context;
import android.support.annotation.NonNull;

/**
 * Created by Hesham on 5/8/2016.
 */
public class AnFPromotions extends Application {

    private static AnFPromotions instance;
    public static boolean online = true;
    private static  String mainPromotionsURL;
    private static PromotionsRepository mPromotionsRepository;
    private static ImageLoader mImageLoader;
    /**
     * 20% of the heap goes to image cache. Stored in kiloibytes.
     */
    private static final int IMAGE_CACHE_SIZE_KB = (int) (Runtime.getRuntime().maxMemory() / 1024 / 5);

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        instance = this;
    }
    public static AnFPromotions getInstance(){
        return instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mainPromotionsURL = getResources().getString(R.string.promotions_url);
    }

    public static String getPromotionsURL(){
        return mainPromotionsURL;
    }

    public static void storeLastFreshRepository(@NonNull PromotionsRepository  repository){
        mPromotionsRepository = repository;
    }

    public static PromotionsRepository getCachedPromoRepo (){
        if(mPromotionsRepository!= null){
            return mPromotionsRepository;
        }else {
            return new PromotionsRepository();
        }
    }

    public ImageLoader getImageLoader() {
        if (mImageLoader == null) {
            mImageLoader = new ImageLoader(Volley.newRequestQueue(this), new BitmapLruCache(IMAGE_CACHE_SIZE_KB));
        }
        return mImageLoader;
    }
}
