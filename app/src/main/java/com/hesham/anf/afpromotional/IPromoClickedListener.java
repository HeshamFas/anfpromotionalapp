package com.hesham.anf.afpromotional;

import com.hesham.anf.afpromotional.data.Promotion;

/**
 * Created by Hesham on 5/11/2016.
 */
public interface IPromoClickedListener {
    void onPromoClicked(Promotion Promo);

}
