package com.hesham.anf.afpromotional;

import com.hesham.anf.afpromotional.data.Promotion;
import com.hesham.anf.afpromotional.detail.PromotionDetailFragment;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.List;

public class PromotionsActivity extends BaseActivity implements PromotionsContract.View,IPromoClickedListener{

    private PromotionsContract.UserActionsListener presenter;
    TextView mTextView ;

    TextView mResultStatus;
    LinearLayout mResultLayout ;
    RelativeLayout mNoResultLayout ;
    RecyclerView mPromotionsContainer ;
    //google search api allows only 10 image at each call
    LinearLayoutManager layoutManager;
    PromotionsAdapter mPromotionsAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mTextView = (TextView) findViewById(R.id.text);
        mResultStatus =(TextView)findViewById(R.id.tv_promotion_result);
        mResultLayout =(LinearLayout) findViewById(R.id.ll_promotions_container);
        mNoResultLayout = (RelativeLayout) findViewById(R.id.rl_promo_no_result);
        mPromotionsContainer = (RecyclerView)findViewById(R.id.rv_promotion_container);
        presenter = new PromotionsPresenter(this);
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                presenter.loadPromotions(true);
                Snackbar.make(view, "Refreshing.....", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

    }

    @Override
    protected void onStart() {
        super.onStart();
        presenter.loadPromotions(true);
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_start, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    public void setProgressIndicator(boolean active) {
            showNoResultLayout(true);
    }

    @Override
    public void showPromotions(List<Promotion> promotions) {
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        mPromotionsContainer.setLayoutManager(layoutManager);
        mPromotionsAdapter = new PromotionsAdapter(this,promotions,this );
        mPromotionsContainer.setAdapter(mPromotionsAdapter);
        showResultLayout();
    }

    @Override
    public void showEmptyPromotions() {

    }

    @Override
    public void showPromotionDetailUi(Promotion requestedPromotion) {
        PromotionDetailFragment detailFragment = new PromotionDetailFragment();
        Bundle arguments = new Bundle();
        arguments.putString("url", requestedPromotion.getPromotionImageUrl());
        arguments.putString("button_title",requestedPromotion.getButtonTitle());
        arguments.putString("button_target", requestedPromotion.getButtonTargetUrl());
        arguments.putString("promo_title",requestedPromotion.getPromoTitle());
        arguments.putString("promo_desc",requestedPromotion.getPromoDescription());
        arguments.putString("promo_footer",requestedPromotion.getPromoFooter());
        detailFragment.setArguments(arguments);
        detailFragment.show(getFragmentManager(), "fragment_edit_name");
    }

    private void showResultLayout(){
        mNoResultLayout.setVisibility(View.GONE);
        mResultLayout.setVisibility(View.VISIBLE);
    }
    private void showNoResultLayout(boolean showProgressBard){
        mNoResultLayout.setVisibility(View.VISIBLE);
        mResultLayout.setVisibility(View.GONE);
    }

    @Override
    public void onPromoClicked(Promotion promo) {
        presenter.openPromotionDetails(promo);
    }
}
