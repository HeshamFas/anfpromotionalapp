package com.hesham.anf.afpromotional;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.hesham.anf.afpromotional.data.Promotion;

import android.content.Context;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Hesham on 5/8/2016.
 */
public class PromotionsAdapter extends RecyclerView.Adapter<PromotionsAdapter.ItemViewHolder>{
    ArrayList<Promotion> mPromotions;
    private Context mContext;
    private ImageLoader mImageLoader = AnFPromotions.getInstance().getImageLoader();
    private IPromoClickedListener mIPromoClickedListener;

    public PromotionsAdapter(Context context, List<Promotion> promotions, IPromoClickedListener promoClickedListener){
        this.mContext = context;
        this.mPromotions = (ArrayList<Promotion>)promotions;
        this.mIPromoClickedListener = promoClickedListener;
    }

    @Override
    public ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View viewItem = inflater.inflate(R.layout.promotion_item, null);
        return new PromotionItemViewHolder(viewItem, this)  ;
    }

    @Override
    public void onBindViewHolder(ItemViewHolder holder, int position) {
        if(mPromotions.get(position) != null){
            Promotion responseItem = mPromotions.get(position);
            holder.populateItemView(responseItem);
        }
    }

    @Override
    public int getItemCount() {
        return mPromotions.size();
    }

    public static abstract class ItemViewHolder extends RecyclerView.ViewHolder{
        View searchItemView;
        PromotionsAdapter mAdapter;

        public ItemViewHolder(View itemView, PromotionsAdapter adapter){
            super(itemView);
            this.searchItemView = itemView;
            this.mAdapter = adapter;
        }
        protected abstract void populateItemView(Promotion currentItem);
    }

    public static class PromotionItemViewHolder extends ItemViewHolder implements AdapterView.OnClickListener{
   /*     NetworkImageView networkImageView;*/
        NetworkImageView mImageView;
        TextView itemTitle;

        Promotion currentItem;
        public PromotionItemViewHolder(View itemView, PromotionsAdapter adapter){
            super(itemView, adapter);
           //networkImageView = (NetworkImageView)itemView.findViewById(R.id.niv_custom_search);

            itemTitle = (TextView)itemView.findViewById(R.id.tv_promotion_title) ;
            mImageView = (NetworkImageView) itemView.findViewById(R.id.niv_promotion_thumb);
            itemView.setOnClickListener(this);
        }

        @Override
        protected void populateItemView(Promotion currentItem) {

            this.currentItem = currentItem;
            itemTitle.setText(currentItem.getPromoTitle());
           mImageView.setImageUrl(currentItem.getPromotionImageUrl(),mAdapter.mImageLoader);
        }

        @Override
        public void onClick(View v) {
            Snackbar.make(itemView,"Loading Detail" ,Snackbar.LENGTH_SHORT).setAction("action",null).show();
                    mAdapter.mIPromoClickedListener.onPromoClicked(currentItem);
        }
    }

}
