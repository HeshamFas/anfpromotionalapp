package com.hesham.anf.afpromotional;

import com.hesham.anf.afpromotional.data.Promotion;

import android.support.annotation.NonNull;

import java.util.List;

/**
 * Created by Hesham on 5/8/2016.
 */
public interface PromotionsContract {
    interface View {

        void setProgressIndicator(boolean active);

        void showPromotions(List<Promotion> promotions);
        void showEmptyPromotions();


        void showPromotionDetailUi(Promotion requestedPromotion);
    }

    interface UserActionsListener {

        void loadPromotions(boolean forceUpdate);

        void openPromotionDetails(@NonNull Promotion requestedPromotion);
    }

}
