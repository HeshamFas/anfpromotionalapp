package com.hesham.anf.afpromotional;

import com.hesham.anf.afpromotional.data.Promotion;
import com.hesham.anf.afpromotional.data.PromotionRepoInjector;
import com.hesham.anf.afpromotional.data.PromotionsRepository;
import com.hesham.anf.afpromotional.service.PromotionRepoInjectorCallBack;

import android.support.annotation.NonNull;

/**
 * Created by Hesham on 5/8/2016.
 */


public class PromotionsPresenter implements PromotionsContract.UserActionsListener,
        PromotionRepoInjectorCallBack{
    private final PromotionsContract.View mPromView ;

    public PromotionsPresenter(@NonNull PromotionsContract.View notesView) {
        mPromView = notesView;
    }

    @Override
    public void loadPromotions(boolean forceUpdate) {
        if(forceUpdate){
                    new PromotionRepoInjector(this).launchPromotionServiceCall();
            mPromView.setProgressIndicator(true);
                }
    }

    @Override
    public void openPromotionDetails(@NonNull Promotion requestedPromotion) {
                    mPromView.showPromotionDetailUi(requestedPromotion);
    }

    @Override
    public void onPromoRepoReceived(PromotionsRepository repository) {
        if(repository.isReady()){
            mPromView.showPromotions(repository.getPromotions());
    }else {
            mPromView.showEmptyPromotions();
        }
    }
}
