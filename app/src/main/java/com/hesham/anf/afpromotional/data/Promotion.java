package com.hesham.anf.afpromotional.data;

/**
 * Created by Hesham on 5/8/2016.
 */
public class Promotion {

    private String buttonTargetUrl;
    private String buttonTitle;
    private String promoDescription;
    private String promoFooter;
    private String promoTitle;
    private String promoImageUrl;

  /*  private PromotionDetail mPromotionDetail= null;*/

    public String getButtonTargetUrl() {
        return buttonTargetUrl;
    }

    public void setButtonTargetUrl(String buttonTargetUrl) {
        this.buttonTargetUrl = buttonTargetUrl;
    }

    public String getButtonTitle() {
        return buttonTitle;
    }

    public void setButtonTitle(String buttonTitle) {
        this.buttonTitle = buttonTitle;
    }

    public String getPromoDescription() {
        return promoDescription;
    }

    public void setPromoDescription(String promoDescription) {
        this.promoDescription = promoDescription;
    }

    public String getPromoFooter() {
        return promoFooter;
    }

    public void setPromoFooter(String promoFooter) {
        this.promoFooter = promoFooter;
    }

    public String getPromoTitle() {
        return promoTitle;
    }

    public void setPromotionTitle(String promoTitle) {
        this.promoTitle = promoTitle;
    }

    public String getPromotionImageUrl() {
        return promoImageUrl;
    }

    public void setPromoImageUrl(String promoImageUrl) {
        this.promoImageUrl = promoImageUrl;
    }

/*    public PromotionDetail getPromotionDetail() {
        return mPromotionDetail;
    }

    public void setPromotionDetail(PromotionDetail promotionDetail) {
        mPromotionDetail = promotionDetail;
    }*/

}
