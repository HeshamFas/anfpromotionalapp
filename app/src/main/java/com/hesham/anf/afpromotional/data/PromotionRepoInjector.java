package com.hesham.anf.afpromotional.data;

import com.hesham.anf.afpromotional.AnFPromotions;
import com.hesham.anf.afpromotional.service.PromotionRepoInjectorCallBack;
import com.hesham.anf.afpromotional.service.PromotionServiceAPI;
import com.hesham.anf.afpromotional.service.PromotionsServiceCall;

import android.support.annotation.NonNull;
import android.util.Log;

/**
 * Created by Hesham on 5/8/2016.
 */
public class PromotionRepoInjector implements PromotionServiceAPI{

    PromotionRepoInjectorCallBack mInjectorCallBack;

   public PromotionRepoInjector(@NonNull PromotionRepoInjectorCallBack callBack){
       mInjectorCallBack = callBack;
   }

    public void launchPromotionServiceCall(){
        new PromotionsServiceCall(this).execute(AnFPromotions.getPromotionsURL());
        Log.d("Hello", this.toString() + "calling promotion service call");
    }

    @Override
    public void onSuccess(PromotionsRepository repository) {
        mInjectorCallBack.onPromoRepoReceived(repository);
        Log.d("Hello", this.toString() + "onSuccess");
        AnFPromotions.storeLastFreshRepository(repository);
    }

    @Override
    public void onFailure() {
        mInjectorCallBack.onPromoRepoReceived(AnFPromotions.getCachedPromoRepo());
    }
}
