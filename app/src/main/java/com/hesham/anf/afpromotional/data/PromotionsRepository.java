package com.hesham.anf.afpromotional.data;

import java.util.ArrayList;

/**
 * Created by Hesham on 5/8/2016.
 */
public class PromotionsRepository {


    private  ArrayList<Promotion> mPromotions;


    public  ArrayList<Promotion> getPromotions() {
        return mPromotions;
    }

    public void setPromotions(
            ArrayList<Promotion> promotions) {
        mPromotions = promotions;
    }

    public boolean isReady(){
        return mPromotions!=null && mPromotions.size()>0;
    }

}
