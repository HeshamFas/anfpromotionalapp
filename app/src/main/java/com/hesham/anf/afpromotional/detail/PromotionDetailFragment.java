package com.hesham.anf.afpromotional.detail;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.hesham.anf.afpromotional.AnFPromotions;
import com.hesham.anf.afpromotional.R;

import android.app.DialogFragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ViewSwitcher;

import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by Hesham on 3/12/2016.
 */
public class PromotionDetailFragment extends DialogFragment implements
        View.OnClickListener{

    ImageLoader mImageLoader = AnFPromotions.getInstance().getImageLoader();
    NetworkImageView mNetworkImageView;
    Button mCloseBtn ;
    Button mTargetButton;
    ViewSwitcher mViewSwitcher;
    private URL imageUrl;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        return View.inflate(getActivity(), R.layout.promotion_detail,container);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mNetworkImageView = (NetworkImageView)view.findViewById(R.id.niv_detail_picture);
        mCloseBtn = (Button) view.findViewById(R.id.btn_detail_close);
        mTargetButton= (Button)view.findViewById(R.id.btn_detail_detail) ;
        mCloseBtn.setOnClickListener(this);
        mTargetButton.setOnClickListener(this);
        mViewSwitcher = (ViewSwitcher)view.findViewById(R.id.vs_detail);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getDialog().setCanceledOnTouchOutside(false);
        Bundle arguments = getArguments();
        String url = arguments.getString("url");
        if(!TextUtils.isEmpty(url)){
            try {
                imageUrl = new URL(url);
                mNetworkImageView.setImageUrl(url,mImageLoader);
            } catch (MalformedURLException e) {
                showNoPictureLayout();
                e.printStackTrace();
            }
        }
/*        arguments.putString("button_title",requestedPromotion.getButtonTitle());
        arguments.putString("button_target", requestedPromotion.getButtonTargetUrl());
        arguments.putString("promo_title",requestedPromotion.getPromoTitle());
        arguments.putString("promo_desc",requestedPromotion.getPromoDescription());
        arguments.putString("promo_footer",requestedPromotion.getPromoFooter());*/
    }

    private void showNoPictureLayout(){
        // ToDo work in progress
        //show when url fails
        // will be a FrameLayout Showing no pictures
    }

    @Override
    public void onClick(View v) {
        this.dismiss();
    }
}
