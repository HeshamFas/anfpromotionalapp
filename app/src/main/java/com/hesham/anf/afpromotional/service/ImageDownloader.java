package com.hesham.anf.afpromotional.service;

import com.android.volley.Response;
import com.android.volley.toolbox.ImageRequest;

import android.graphics.Bitmap;
import android.widget.ImageView;

/**
 * Created by Hesham on 5/10/2016.
 */
public class ImageDownloader {
    String mUrl;
    Response.Listener<Bitmap> mResponseListener;
    Response.ErrorListener mResponseErrorListener;

    public ImageDownloader (Response.Listener<Bitmap> bitmapResponse){
        this.mResponseListener = bitmapResponse;
    }

    public void download(String url){
        ImageRequest imgRequest = new ImageRequest(url, mResponseListener,0,0, ImageView.ScaleType.FIT_XY, Bitmap.Config.ARGB_8888,mResponseErrorListener);

}}
