package com.hesham.anf.afpromotional.service;

import com.hesham.anf.afpromotional.data.PromotionsRepository;

/**
 * Created by Hesham on 5/8/2016.
 */
public interface PromotionRepoInjectorCallBack {

    void onPromoRepoReceived(PromotionsRepository repository);
}
