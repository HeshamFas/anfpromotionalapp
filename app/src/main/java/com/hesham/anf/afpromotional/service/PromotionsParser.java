package com.hesham.anf.afpromotional.service;

import com.hesham.anf.afpromotional.data.Promotion;
import com.hesham.anf.afpromotional.data.PromotionsRepository;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

/**
 * Created by Hesham on 5/8/2016.
 */
public class PromotionsParser {

    private static final  String PROMOTIONS = "promotions";
    private static final String BUTTON = "button";
    private static final String PROMOTION_BTN_TARGET ="target";
    private static final String PROMOTION_BTN_TITLE ="title";
    private static final String PROMOTION_DESCRIPTION="description";
    private static final String PROMOTION_FOOTER="footer";
    private static final String PROMOTION_IMAGE_URL="image";
    private static final String PROMOTION_TITLE="title";

    public static PromotionsRepository parse(InputStream is) throws IOException,
            JSONException {
        PromotionsRepository promotionsRepository = new PromotionsRepository();
        ArrayList<Promotion> promotions = new ArrayList<>();
        Promotion promotionItem;
        String result = convertStreamToString(is);
        if (result != null) {

            if(result.contains("\n")){
                result.replaceAll("\n ","");
            }
            JSONObject mainResponse = new JSONObject(result);
            JSONArray promotionsArray = mainResponse.getJSONArray(PROMOTIONS);
            if(promotionsArray!= null ){
                for (int i = 0; i < promotionsArray.length(); i++) {
                    JSONObject tempObject = promotionsArray.getJSONObject(i);
                    if (tempObject != null) {
                        promotionItem = new Promotion();
                        JSONObject buttonObject=null;
                        if(!tempObject.isNull(BUTTON)){
                            Object testObject = tempObject.get(BUTTON);
                            if(testObject instanceof JSONObject){
                                buttonObject = tempObject.getJSONObject(BUTTON);
                            }else if(testObject instanceof JSONArray) {
                                buttonObject = tempObject.getJSONArray("button").getJSONObject(0);
                            }
                        }
                        if(buttonObject!=null){
                            String buttonTarget = buttonObject.getString(PROMOTION_BTN_TARGET);
                            String buttonTitle = buttonObject.getString(PROMOTION_BTN_TITLE);
                            promotionItem.setButtonTargetUrl(buttonTarget!=null ? buttonTarget:"");
                            promotionItem.setButtonTitle(buttonTitle!=null?buttonTitle:"");
                        }
                        if(!tempObject.isNull(PROMOTION_DESCRIPTION)){
                            String description = tempObject.getString(PROMOTION_DESCRIPTION);
                            promotionItem.setPromoDescription(description!= null? description: "");
                        }
                        if(!tempObject.isNull(PROMOTION_FOOTER)){
                            String footer = tempObject.getString(PROMOTION_FOOTER);
                            promotionItem.setPromoFooter(footer!= null? footer: "");
                        }
                        if(!tempObject.isNull(PROMOTION_IMAGE_URL)){
                                String image = tempObject.getString(PROMOTION_IMAGE_URL);
                                promotionItem.setPromoImageUrl(image!= null? image: "");
                            }
                        if(!tempObject.isNull(PROMOTION_TITLE)){
                                    String title = tempObject.getString(PROMOTION_TITLE);
                                    promotionItem.setPromotionTitle(title!= null? title: "");
                                }
                        promotions.add(promotionItem);

                    }
                }}else {
               //TODO call failed callback
            }
            promotionsRepository.setPromotions(promotions);

        }else {
            // TODO call failed callback
            //PromotionsRepository.setIsSuccessfull(false);
        }

        return promotionsRepository;
    }


    public static String convertStreamToString(InputStream is) throws IOException {
        StringBuilder sb = new StringBuilder();
        if (is != null) {
            BufferedReader reader = new BufferedReader(new InputStreamReader(is));
            String line = null;
            try {
                while ((line = reader.readLine()) != null) {
                    sb.append(line + "\n");
                }
            } finally {
                is.close();
            }
        }
        return sb.toString();
    }

}
