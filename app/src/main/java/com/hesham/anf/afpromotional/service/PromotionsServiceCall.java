package com.hesham.anf.afpromotional.service;

import com.hesham.anf.afpromotional.data.PromotionsRepository;

import org.json.JSONException;

import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by Hesham on 5/8/2016.
 */
public class PromotionsServiceCall  extends AsyncTask<String,Void, PromotionsRepository>{

    private PromotionServiceAPI mPromotionServiceAPI;

    public PromotionsServiceCall(@NonNull PromotionServiceAPI promotionServiceAPI){
        this.mPromotionServiceAPI = promotionServiceAPI;
    }
    @Override
    protected PromotionsRepository doInBackground(String... params) {
        Log.d("Hello", this.toString() + "do in the background");
        PromotionsRepository output=null;
        HttpURLConnection conn = null;
        try {
            URL url = new URL(params[0]);
            conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Accept", "application/json");
            conn.connect();
            InputStream is = conn.getInputStream();
            output = PromotionsParser.parse(is);
            conn.disconnect();
        } catch (IOException e) {
            Log.d("Hello", this.toString() + "do in the background IO Exception");
            e.printStackTrace();
        } catch (JSONException e) {
            Log.d("Hello", this.toString() + "do in the background JSON Exception");
            e.printStackTrace();
        }finally {
            return output;
        }
    }

    @Override
    protected void onPostExecute(PromotionsRepository promotionsRepository) {
        super.onPostExecute(promotionsRepository);
            if(promotionsRepository!= null && promotionsRepository.isReady()){
                mPromotionServiceAPI.onSuccess(promotionsRepository);
            }else {
                mPromotionServiceAPI.onFailure();
            }

    }
}
